source venv-mattermost/bin/activate.fish
set -x DATABASE_HOST ""
set -x DATABASE_PASS ""
set -x DATABASE_USER ""
set -x DEFAULT_FROM_EMAIL "web@pkimber.net"
set -x DJANGO_SETTINGS_MODULE "example_mattermost.dev_patrick"
set -x HOST_NAME "http://localhost/"
set -x MAIL_TEMPLATE_TYPE "django"
set -x SECRET_KEY "the_secret_key"
source .private
