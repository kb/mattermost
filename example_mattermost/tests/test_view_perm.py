# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse

from login.tests.fixture import perm_check


@pytest.mark.django_db
def test_dash(perm_check):
    perm_check.staff(reverse("project.dash"))


@pytest.mark.django_db
def test_home(perm_check):
    perm_check.anon(reverse("project.home"))


@pytest.mark.django_db
def test_settings(perm_check):
    perm_check.staff(reverse("project.settings"))
