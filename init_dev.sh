#!/bin/bash
# exit immediately if a command exits with a nonzero exit status.
set -e
# treat unset variables as an error when substituting.
set -u

DB_NAME="dev_test_mattermost_`id -nu`"

psql -X -U postgres -c "DROP DATABASE IF EXISTS ${DB_NAME};"
psql -X -U postgres -c "CREATE DATABASE ${DB_NAME} TEMPLATE=template0 ENCODING='utf-8';"

# pytest -x
# touch temp.db && rm temp.db
django-admin.py migrate --noinput
django-admin.py demo_data_login
django-admin.py init_app_mattermost
django-admin.py demo_data_mattermost
django-admin.py runserver
