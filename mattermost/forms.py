# -*- encoding: utf-8 -*-
from base.form_utils import RequiredFieldForm
from .models import Channel


class ChannelForm(RequiredFieldForm):
    class Meta:
        model = Channel
        fields = ["url"]


class ChannelEmptyForm(RequiredFieldForm):
    class Meta:
        model = Channel
        fields = []
