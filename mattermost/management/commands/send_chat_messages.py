# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from mattermost.tasks import send_chat_messages


class Command(BaseCommand):

    help = "Send chat messages..."

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        count = send_chat_messages()
        self.stdout.write(
            "Sent {} chat messages - Complete".format(count, self.help)
        )
