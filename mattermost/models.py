# -*- encoding: utf-8 -*-
import logging
import requests

from django.conf import settings
from django.db import DatabaseError, models, transaction
from http import HTTPStatus

from base.model_utils import (
    RetryModel,
    RetryModelManager,
    TimedCreateModifyDeleteModel,
)


logger = logging.getLogger(__name__)


class MattermostError(Exception):
    def __init__(self, value):
        Exception.__init__(self)
        self.value = value

    def __str__(self):
        return repr("%s, %s" % (self.__class__.__name__, self.value))


class ChannelManager(models.Manager):
    def _create_channel(self, slug, url):
        x = self.model(slug=slug, url=url)
        x.save()
        return x

    def current(self):
        return self.model.objects.all().exclude(deleted=True)

    def init_channel(self, slug, url):
        try:
            x = self.model.objects.get(slug=slug)
            x.url = url
            x.save()
        except self.model.DoesNotExist:
            x = self._create_channel(slug, url)
        return x


class Channel(TimedCreateModifyDeleteModel):

    ACTIVITY = "activity"

    slug = models.SlugField(max_length=100, unique=True)
    url = models.URLField(max_length=200)

    objects = ChannelManager()

    class Meta:
        ordering = ("slug", "url")

    def __str__(self):
        return "{}: {}".format(self.slug, self.url)


class MessageModelManager(RetryModelManager):
    def create_message(self, channel, user, title, description):
        x = self.model(
            channel=channel, user=user, title=title, description=description
        )
        x.save()
        return x

    def current(self):
        return self.model.objects.all()

    def send(self):
        result = []
        pks = [x.pk for x in self.outstanding()]
        for pk in pks:
            with transaction.atomic():
                try:
                    # only send once (if this function is called twice)
                    message = self.model.objects.select_for_update(
                        nowait=True
                    ).get(pk=pk)
                    response = requests.post(
                        message.channel.url,
                        json={
                            "channel": message.channel.slug,
                            "username": message.user.username,
                            "text": message.title,
                        },
                    )
                    if HTTPStatus.CREATED == response.status_code:
                        message.set_complete()
                        result.append(message.pk)
                    else:
                        message.set_fail()
                        logger.error(
                            "Cannot post message {} to Mattermost, "
                            "{}: {}".format(
                                message.pk, response.status_code, str(response)
                            )
                        )
                except DatabaseError:
                    # record is locked, so leave alone this time
                    pass
        return result


class Message(RetryModel):
    """Chat message waiting to be sent."""

    created = models.DateTimeField(auto_now_add=True)
    channel = models.ForeignKey(Channel, on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    description = models.TextField()
    objects = MessageModelManager()

    class Meta:
        ordering = ("-pk",)
        verbose_name = "Chat Message"

    def __str__(self):
        result = "{} [{}]".format(self.title, self.channel.slug)
        retry_str = self.retry_str()
        if retry_str:
            result = "{} ({})".format(result, retry_str)
        return result

    @staticmethod
    def get_max_retry_count():
        return 10
