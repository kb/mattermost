# -*- encoding: utf-8 -*-
import logging

from celery import task

from .models import Message


logger = logging.getLogger(__name__)


@task()
def send_chat_messages():
    logger.info("Send chat messages...")
    result = Message.objects.send()
    logger.info("Sent {} chat messages...".format(len(result)))
    return len(result)
