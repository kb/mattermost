# -*- encoding: utf-8 -*-
import factory

from login.tests.factories import UserFactory
from mattermost.models import Channel, Message


class ChannelFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Channel

    url = "https://chat.hatherleigh.info"

    @factory.sequence
    def slug(n):
        return "slug_{}".format(n + 1)


class MessageFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Message

    channel = factory.SubFactory(ChannelFactory)
    user = factory.SubFactory(UserFactory)

    @factory.sequence
    def title(n):
        return "title_{}".format(n + 1)
