# -*- encoding: utf-8 -*-
import pytest

from login.tests.factories import UserFactory
from mattermost.models import Channel
from mattermost.tests.factories import ChannelFactory


@pytest.mark.django_db
def test_current():
    ChannelFactory(slug="c")
    channel = ChannelFactory(slug="b")
    channel.set_deleted(UserFactory())
    ChannelFactory(slug="a")
    assert ["a", "c"] == [x.slug for x in Channel.objects.current()]


@pytest.mark.django_db
def test_init_channel():
    channel = Channel.objects.init_channel(
        Channel.ACTIVITY, "https://chat.hatherleigh.info"
    )
    assert Channel.ACTIVITY == channel.slug
    assert "https://chat.hatherleigh.info" == channel.url


@pytest.mark.django_db
def test_init_channel_already_exists():
    ChannelFactory(slug=Channel.ACTIVITY, url="https://chat.hatherleigh.info")
    assert 1 == Channel.objects.count()
    channel = Channel.objects.init_channel(
        Channel.ACTIVITY, "https://chat.okehampton.net"
    )
    assert 1 == Channel.objects.count()
    assert Channel.ACTIVITY == channel.slug
    assert "https://chat.okehampton.net" == channel.url


@pytest.mark.django_db
def test_str():
    channel = ChannelFactory(
        slug="Dylan-Activity", url="https://thisisawebsite.com"
    )
    assert "Dylan-Activity: https://thisisawebsite.com" == str(channel)


@pytest.mark.django_db
def test_ordering():
    ChannelFactory(slug="c")
    ChannelFactory(slug="b")
    ChannelFactory(slug="a")
    assert ["a", "b", "c"] == [x.slug for x in Channel.objects.current()]
