# -*- encoding: utf-8 -*-
import pytest
import responses

from datetime import date
from django.utils import timezone
from freezegun import freeze_time
from http import HTTPStatus
from unittest import mock

from login.tests.factories import UserFactory
from mattermost.models import Channel, Message
from mattermost.tests.factories import ChannelFactory, MessageFactory


@pytest.mark.django_db
def test_create_message():
    with freeze_time("2017-05-21 23:55:01"):
        message = Message.objects.create_message(
            ChannelFactory(slug=Channel.ACTIVITY),
            UserFactory(username="patrick"),
            "Apple",
            "Fruit",
        )
    assert date(2017, 5, 21) == message.created.date()
    assert "patrick" == message.user.username
    assert "Apple" == message.title
    assert "Fruit" == message.description


@pytest.mark.django_db
def test_current():
    MessageFactory(title="a")
    MessageFactory(title="b")
    assert ["b", "a"] == [x.title for x in Message.objects.current()]


@pytest.mark.django_db
def test_ordering():
    MessageFactory(title="a")
    MessageFactory(title="b")
    assert ["b", "a"] == [x.title for x in Message.objects.all()]


@pytest.mark.django_db
@responses.activate
def test_send():
    responses.add(
        responses.POST,
        "https://chat.hatherleigh.info",
        json={"Status": "OK"},
        status=HTTPStatus.CREATED,
    )
    channel = ChannelFactory(slug=Channel.ACTIVITY)
    message_1 = MessageFactory(channel=channel, title="a", retries=3)
    MessageFactory(channel=channel, title="b", completed_date=timezone.now())
    message_2 = MessageFactory(channel=channel, title="c")
    MessageFactory(channel=channel, title="d", retries=99)
    assert [message_1.pk, message_2.pk] == Message.objects.send()


@pytest.mark.django_db
def test_str():
    message = MessageFactory(
        channel=ChannelFactory(slug=Channel.ACTIVITY),
        title="Patrick",
        retries=3,
    )
    assert "Patrick [activity] (retry x 3)" == str(message)
