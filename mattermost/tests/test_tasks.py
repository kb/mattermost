# -*- encoding: utf-8 -*-
import pytest
import responses

from django.utils import timezone
from http import HTTPStatus
from unittest import mock

from mattermost.models import Channel
from mattermost.tasks import send_chat_messages
from mattermost.tests.factories import ChannelFactory, MessageFactory


@pytest.mark.django_db
@responses.activate
def test_send_chat_messages():
    responses.add(
        responses.POST,
        "https://chat.hatherleigh.info",
        json={"Status": "OK"},
        status=HTTPStatus.CREATED,
    )
    channel = ChannelFactory(slug=Channel.ACTIVITY)
    MessageFactory(channel=channel, title="a", retries=3)
    MessageFactory(channel=channel, title="b", completed_date=timezone.now())
    MessageFactory(channel=channel, title="c")
    MessageFactory(channel=channel, title="d", retries=99)
    assert 2 == send_chat_messages()
