# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse
from http import HTTPStatus

from login.tests.factories import TEST_PASSWORD, UserFactory
from mattermost.models import Channel
from mattermost.tests.factories import ChannelFactory


@pytest.mark.django_db
def test_channel_list(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    ChannelFactory(slug="c")
    channel = ChannelFactory(slug="b")
    channel.set_deleted(user)
    ChannelFactory(slug="a")
    response = client.get(reverse("mattermost.channel.list"))
    assert HTTPStatus.OK == response.status_code
    assert "channel_list" in response.context
    channel_list = response.context["channel_list"]
    assert ["a", "c"] == [x.slug for x in channel_list]


@pytest.mark.django_db
def test_channel_update(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    channel = ChannelFactory(
        slug=Channel.ACTIVITY, url="https://thisisawebsite.com"
    )
    response = client.post(
        reverse("mattermost.channel.update", args=[channel.pk]),
        {"url": "https://chat.hatherleigh.info"},
    )
    assert HTTPStatus.FOUND == response.status_code
    channel.refresh_from_db()
    assert Channel.ACTIVITY == channel.slug
    assert "https://chat.hatherleigh.info" == channel.url


@pytest.mark.django_db
def test_channel_delete(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    channel = ChannelFactory(
        slug=Channel.ACTIVITY, url="https://chat.hatherleigh.info"
    )
    assert channel.is_deleted is False
    response = client.post(
        reverse("mattermost.channel.delete", args=[channel.pk])
    )
    assert HTTPStatus.FOUND == response.status_code
    channel.refresh_from_db()
    assert channel.is_deleted is True
