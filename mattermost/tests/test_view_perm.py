# -*- encoding: utf-8 -*-
import pytest

from datetime import date
from django.urls import reverse

from login.tests.fixture import perm_check
from mattermost.models import Channel
from mattermost.tests.factories import ChannelFactory


@pytest.mark.django_db
def test_channel_list(perm_check):
    url = reverse("mattermost.channel.list")
    perm_check.staff(url)


@pytest.mark.django_db
def test_channel_update(perm_check):
    channel = ChannelFactory()
    url = reverse("mattermost.channel.update", args=[channel.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_channel_delete(perm_check):
    channel = ChannelFactory()
    url = reverse("mattermost.channel.delete", args=[channel.pk])
    perm_check.staff(url)
