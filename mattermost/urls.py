# -*- encoding: utf-8 -*-
from django.conf.urls import url

from .views import ChannelDeleteView, ChannelListView, ChannelUpdateView


urlpatterns = [
    url(
        regex=r"^channel/$",
        view=ChannelListView.as_view(),
        name="mattermost.channel.list",
    ),
    url(
        regex=r"^channel/(?P<pk>\d+)/update/$",
        view=ChannelUpdateView.as_view(),
        name="mattermost.channel.update",
    ),
    url(
        regex=r"^channel/(?P<pk>\d+)/delete/$",
        view=ChannelDeleteView.as_view(),
        name="mattermost.channel.delete",
    ),
]
