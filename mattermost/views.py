# -*- encoding: utf-8 -*-
from braces.views import LoginRequiredMixin, StaffuserRequiredMixin
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views.generic import ListView, UpdateView

from base.view_utils import BaseMixin
from mattermost.forms import ChannelForm, ChannelEmptyForm
from .models import Channel


class ChannelListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):
    def get_queryset(self):
        return Channel.objects.current()


class ChannelUpdateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):
    form_class = ChannelForm
    model = Channel

    def get_success_url(self):
        return reverse("mattermost.channel.list")


class ChannelDeleteView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):
    form_class = ChannelEmptyForm
    model = Channel

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.set_deleted(self.request.user)
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse("mattermost.channel.list")
